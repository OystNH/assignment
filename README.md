# MovieFranchiseAPI
Assignment submission: Web API

ASP.NET Core Web API and Entity Framework Code First workflow

## Getting Started
Clone to a local directory.
Open solution in Visual Studio
Run migrations
Run IIS Server

## Prerequisites
.NET Framework 5.0
Visual Studio 2017/19 OR Visual Studio Code

## Authors
* Øystein Herskedal
