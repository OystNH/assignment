﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Models
{
    [Table("Franchise")]
    public class Franchise
    {

        // PK
        public int Id { get; set; }
        [Required]
        [MaxLength(60)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }



        // Relationships
        // one to many
        public ICollection<Movie> Movies { get; set; }
        public ICollection<Character> Characters { get; set; }





    }
}
