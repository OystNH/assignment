﻿using AutoMapper;
using MovieFranchiseAPI.Models.DTOs.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Models.Profiles
{
    public class CharacterProfile: Profile
    {

        public CharacterProfile()
        {

            CreateMap<Character, CharacterReadDTO>()
                .ForMember(chdto => chdto.Movies, opt => opt
                .MapFrom(ch => ch.Movies.Select(ch => ch.Id).ToArray()));


            CreateMap<CharacterCreateDTO, Character>();
            CreateMap<CharacterEditDTO, Character>();
        }
    }
}
