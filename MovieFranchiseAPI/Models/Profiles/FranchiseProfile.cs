﻿using AutoMapper;
using MovieFranchiseAPI.Models.DTOs.Character;
using MovieFranchiseAPI.Models.DTOs.Franchise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Models.Profiles
{
    public class FranchiseProfile: Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                
                .ForMember(fcdto => fcdto.Movies, opt => opt
                .MapFrom(fc => fc.Movies.Select(mv => mv.Id).ToArray()));
                



            CreateMap<FranchiseCreateDTO, Franchise>();
            CreateMap<FranchiseEditDTO, Franchise>();
        }
    }
}
