﻿using AutoMapper;
using MovieFranchiseAPI.Models.DTOs.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Models.Profiles
{
    public class MovieProfile: Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mvdto => mvdto.Franchise, opt => opt
                .MapFrom(mv => mv.FranchiseId))

                .ForMember(mvdto => mvdto.Characters, opt => opt
                .MapFrom(mv => mv.Characters.Select(mv => mv.Id).ToArray()));

            CreateMap<MovieCreateDTO, Movie>();

            CreateMap<MovieEditDTO, Movie>();
        }
    }
}
