﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Models.DTOs.Movie
{
    public class MovieCreateDTO
    {

        [MaxLength(60)]
        [Required]
        public string MovieTitle { get; set; }
        [MaxLength(60)]
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(60)]
        public string Director { get; set; }
        
    } 
}
