﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Models.DTOs.Movie
{
    public class MovieReadDTO
    {
        public int Id { get; set; }
        [MaxLength(60)]
        [Required]
        public string MovieTitle { get; set; }
        [MaxLength(60)]
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(60)]
        public string Director { get; set; }
        [MaxLength(200)]
        public string Picture { get; set; }
        [MaxLength(200)]
        public string Trailer { get; set; }

        // Relationships

        // one to many
        public int Franchise { get; set; }

        // many to many
        public int[] Characters { get; set; }

    }
}
