﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Models.DTOs.Franchise
{
    public class FranchiseCreateDTO
    {

        [Required]
        [MaxLength(60)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
    }
}
