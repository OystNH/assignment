﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Models.DTOs.Character
{
    public class CharacterEditDTO
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(60)]
        public string FullName { get; set; }
        [MaxLength(60)]
        public string Alias { get; set; }
        [MaxLength(60)]
        public string Gender { get; set; }
        [MaxLength(200)]
        public string Picture { get; set; }

    }
}
