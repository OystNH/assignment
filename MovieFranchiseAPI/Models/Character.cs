﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Models
{
    [Table("Character")]
    public class Character
    {
        // PK
        public int Id { get; set; }
        [Required]
        [MaxLength(60)]
        public string FullName { get; set; }
        [MaxLength(60)]
        public string Alias { get; set; }
        [MaxLength(60)]
        public string Gender { get; set; }
        [MaxLength(200)]
        public string Picture { get; set; }


        // Relationships


        // many to many

        public ICollection<Movie> Movies { get; set; }




    }
}
