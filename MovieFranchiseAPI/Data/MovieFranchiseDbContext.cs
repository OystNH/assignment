﻿using Microsoft.EntityFrameworkCore;
using MovieFranchiseAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Data
{
    public class MovieFranchiseDbContext: DbContext
    {
        public MovieFranchiseDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 1, MovieTitle = "Harry Potter and the Philosopher's Stone", ReleaseYear = 2001, Genre = "Fantasy/Adventure", Director = "Chris Columbus", Picture = "https://m.media-amazon.com/images/I/71x1RHSaEhL._AC_SY879_.jpg", Trailer = "https://www.youtube.com/watch?v=VyHV0BRtdxo", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 2, MovieTitle = "Spider-Man 2", ReleaseYear = 2002, Genre = "Action/Sci-fi", Director = "Sam Raimi", Picture = "https://m.media-amazon.com/images/I/51OzUVuru0L._AC_.jpg", Trailer = "https://www.youtube.com/watch?v=3jBFwltrxJw", FranchiseId = 2 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 3, MovieTitle = "The Lord of the Rings: The Return of the King", ReleaseYear = 2003, Genre = "Fantasy/Adventure", Director = "Peter Jackson", Picture = "https://m.media-amazon.com/images/I/71X6YzwV0gL._AC_SY879_.jpg", Trailer = "https://www.youtube.com/watch?v=y2rYRu8UW8M", FranchiseId = 3 });


            modelBuilder.Entity<Character>().HasData(new Character { Id = 1, FullName = "Tom Marvolo Riddle", Gender = "Male", Alias = "Lord Voldemort", Picture = "https://upload.wikimedia.org/wikipedia/en/a/a3/Lordvoldemort.jpg"});   
            modelBuilder.Entity<Character>().HasData(new Character { Id = 2, FullName = "Peter Parker", Gender = "Male", Alias = "Spider-Man", Picture = "https://m.media-amazon.com/images/M/MV5BMTcxODg2NjI2N15BMl5BanBnXkFtZTgwMjY3OTUxMTI@._V1_.jpg" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 3, FullName = "Galadriel", Gender = "Female", Alias = "Lady of Light", Picture = "https://static.wikia.nocookie.net/lotr/images/c/cb/Galadriel.jpg/revision/latest/scale-to-width-down/644?cb=20151015204512" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 4, FullName = "Frodo Baggins", Gender = "Male", Alias = "Ring-Bearer", Picture = "https://upload.wikimedia.org/wikipedia/en/4/4e/Elijah_Wood_as_Frodo_Baggins.png" });


            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 1, Name = "Harry Potter", Description = "Harry Potter is a film series based on the eponymous novels by J. K. Rowling. The series is produced and distributed by Warner Bros. Pictures and consists of eight fantasy films, beginning with Harry Potter and the Philosopher's Stone and culminating with Harry Potter and the Deathly Hallows – Part 2." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 2, Name = "Raimi Spider-Man Trilogy", Description = "The Raimi Spider-Man Trilogy are a series of three films directed by Sam Raimi and is based on the Marvel Comics character of the same name. The trilogy focuses on Spider-Man (Peter Parker) as he fights crime in New York City while balancing his normal life." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 3, Name = "Middle Earth", Description = "Professor J. R. R. Tolkien's fantasy novels The Hobbit and The Lord of the Rings, which tell the story of a mythical war and heroic quests set in the lands of Middle-earth, have been the subject of various film and TV adaptations, chiefly six feature films produced, written and directed by Sir Peter Jackson from 2001 to 2015, a tie-in TV show by Amazon slated for release in 2021, and a Biopic of Tolkien's released in 2019." });



            // Seed m2m movie-character. Need to define m2m and access linking table
            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacter",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("MovieId", "CharacterId");
                        je.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 2, CharacterId = 2 },
                            new { MovieId = 3, CharacterId = 3 },
                            new { MovieId = 3, CharacterId = 4 }
                        );
                    });

        }
    }
}
